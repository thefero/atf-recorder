const App = (() => {
    class App {
        init() {
            const _this = this;
            
            $(document)
                .on('mouseover.atf-rec', function(ev) {
                    _this._hoverTimeout = window.setTimeout(() => {
                        _this._hoverTimeout = null;

                        // console.log("Hover");
                    }, 1000);
                    // console.log(this);
                })
                .on('mouseout.atf-rec', function(ev) {
                    clearTimeout(_this._hoverTimeout);
                    // console.log("Out");
                });
        }
    }

    return App;
})();

export default App;