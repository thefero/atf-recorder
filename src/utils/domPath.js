window.$ = window.jQuery = require('jquery');

(function($){
    var getStringForElement = function (el) {
        var string = el.tagName.toLowerCase()
            , classSelector = el.getAttribute('class')
            , $el = $(el);

        if (classSelector) {
            classSelector = '.' + classSelector.replace(/  +/g, '.');
        }

        if (el.getAttribute('id')) {
            string = "#" + el.getAttribute('id');
        } else if ($el.siblings(string).length) {
            if ($el.siblings(classSelector ? classSelector : string).length) {
                let indexOfElement = ($el.parent().children(string).index(el) + 1)
                    , pinpointEl = (indexOfElement === 1) ? ':first-child' : ':nth-child(' + indexOfElement + ')';

                string += pinpointEl;
            } else if (classSelector) {
                string += classSelector;
            } else {
                string += string;
            }
        }

        return string;
    };

    $.fn.getDomPath = function(string = true) {
        let p = [],
            el = $(this).first();

        p.push(getStringForElement(el[0]));
        if (!el[0].id) {
            el.parents().not('html').each(function() {
                p.push(getStringForElement(this));
                if (this.id) {
                    return false;
                } 
            });
        }

        p = p.reverse();

        return string ? p.join(" > ") : p;
    };
})(jQuery);