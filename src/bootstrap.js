/**
* Load Bootstrap 4.0.0-beta.2
*
* info: Bootstrap has jQuery and Popper dependencies and
*   they are loaded by webpack. See: /webpack.config.js
* info: Does not export anything
**/
import 'bootstrap';

/**
* Load Font Awesome SCSS
* This is done after webpack copied the fonts to the static/ direcotry
* See: /webpack.config.js
**/
import 'font-awesome/scss/font-awesome.scss';

/**
* Load the default scss for the project
**/
import './main.scss';