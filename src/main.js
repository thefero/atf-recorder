/**
* bootstrap.js loads up default dependencies.
* You can edit it or remove it all together
**/
require('./bootstrap');

import DOMNode from './components/DOMNode';
import Popover from './components/Popover';

import Click from './components/commands/Click';
// import ClickWithRigthButton from './components/commands/ClickWithRigthButton';
import ClickWithLeftButton from './components/commands/ClickWithLeftButton';
import DoubleClick from './components/commands/DoubleClick';
import RightClick from './components/commands/RightClick';

import App from './App';

// Testing

const popover = new Popover();

$(document.body).append(popover.template);
let el = document.getElementsByClassName('navbar');

// Not testing

const app = new App();
app.init();

const element = new DOMNode(el, {
    onClick() {
        let command = new ClickWithLeftButton(element);
        popover.addCommand(command);
    }

    , onDblClick() {
        let command = new DoubleClick(element);
        popover.addCommand(command);
    }

    , onRightClick() {
        let command = new RightClick(element);
        popover.addCommand(command);
    }
});

popover.setElement(element.pill());