import Click from './commands/Click';

const Command = (() => {
    const COMMAND_CLASSES   = {
                commandContainer    : 'recorder-command-pill'
                , command           : 'recorder-command'
                , actions           : 'recorder-command-actions'
            }
        , PILL_TEMPLATE     = `
            <div class="${COMMAND_CLASSES.commandContainer} recorder-flex-list-item">
                <div class="recorder-flex-start">
                    <button role="button" class="recorder-move-handler">
                        <i class="fa fa-drag-handle" aria-hidden="true"></i>
                    </button>
                </div>
                <span class="${COMMAND_CLASSES.command} recorder-flex-content"></span>
                <div class="${COMMAND_CLASSES.actions} recorder-flex-end">
                    <button class="recorder-transparent-button" role="button">
                        <i class="fa fa-trash-o recorder-hidden-button-icon" aria-hidden="true"></i> Remove
                    </button>
                </div>
            </div>`
        , DEFAULT_OPTIONS   = {};

    class Command {
        constructor(element) {
            this._pillTemplate = $(PILL_TEMPLATE);
            this._element = element;

            return this;
        }

        pill() {
            return this._pillTemplate
                .find(`.${COMMAND_CLASSES.command}`)
                .text(this.constructor.name)
                .end();
        }
    }

    return Command;
})($);

export default Command;