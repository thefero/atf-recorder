import $ from 'jquery';
import '../utils/domPath';

const DOMNode = (() => {

    const PILL_CLASSES  = {
                            container: 'recorder-element-pill'
                        }
    , PILL_TEMPLATE     = `<div class="${PILL_CLASSES.container}"></div>`
    , DEFAULT_OPTIONS   = {
                            onClick() {}
                            , onDblClick() {}
                            , onRightClick() {}
                        }
    , EVENTS            = {
                            click: 'click.recorder'
                            , doubleClick: 'dblclick.recorder'
                            , rightClick: 'mousedown.recorder'
                        }

    class DOMNode {

        constructor(el, options) {
            try {
                this._validateElement(el);
            } catch (er) {
                throw new Error(er.message);
            }

            this._options = $.extend({}, DEFAULT_OPTIONS, options);

            this._template = $(PILL_TEMPLATE);
            this._element = el[0];
            this._$element = $(this._element);
            this._pillHTML = this._createPillHTML();
            this._bindListeners();
        }

        // Public
        props() {
            return {
                element: this._element
                , tag: this._element.tagName
                , clickable: this._isClickable()
                , text: this._$element.clone().children().remove().text().trim()
                , classes: this._element.className
                , selector: this._$element.getDomPath()
            }
        }

        pill() {
            return this._pillHTML;
        }

        _isClickable() {
            return ['button', 'a', 'img'].indexOf(this._element.tagName.toLowerCase()) > -1;
        }

        getActions() {
            // if ()
        }

        getAssertions() {

        }

        // Private
        _validateElement(el) {
            if (!el || !el instanceof HTMLElement || !el.length) {
                throw new Error(`You need to pass a valid DOM element to ${this.constructor.name}`);
            }

            if (el.length > 1) {
                throw new Error(`You need to pass only one DOM element to ${this.constructor.name}`);
            }

            return true;
        }

        _createPillHTML() {
            return this._template.html(`

                <div class="card" style="width: 20rem;">
                    <div class="card-body">
                        <h4 class="card-title">${this.props().tag}</h4>
                        <h6 class="card-subtitle mb-2 text-muted">${this.props().selector}</h6>
                    
                        <p class="card-text">
                                <strong>Classes: </strong>${this.props().classes}<br>
                                <strong>Text: </strong>${this.props().text}
                        </p>
                    </div>
                </div>
            `);
        }

        _bindListeners() {
            this._$element.data('double', 0);

            this._$element
                .bindFirst(EVENTS.click, (ev, next) => {
                    setTimeout(() => {
                        let dblClick = parseInt(this._$element.data('double'), 10);

                        if (dblClick > 0) {
                            this._$element.data('double', --dblClick)
                        } else {
                            this._options.onClick();

                            if (typeof next === 'function') {
                                next();
                            }
                        }
                    }, 300);

                    return false;
                })

                .bindFirst(EVENTS.doubleClick, function (ev, next) {
                    this._$element.data('double', 2);
                    this._options.onDblClick();
                    
                    if (typeof next === 'function') {
                        next();
                    }
                }.bind(this))

                .bindFirst(EVENTS.rightClick, function (ev) {
                    if (ev.button === 2) {
                        this._options.onRightClick();
                    }
                    
                    if (typeof next === 'function') {
                        next();
                    }
                }.bind(this));
        }

    }

    $.fn.bindFirst = function(type, handler) {
        type = type.split(/\s+/);

        return this.each(() => {
            let len = type.length
                , events = $._data(this[0]).events;

            while (len--) {
                let typeNoNamespace = type[len].split('.')[0];

                if (typeof events !== 'undefined' && typeNoNamespace in events) {
                    let element = this
                        , handlers = events[typeNoNamespace].map(function(ev) {
                            return ev.handler;
                        });

                    this
                        .off(typeNoNamespace)
                        .on(type[len], (ev) => {
                            handler.call(this, ev, () => {
                                handlers.forEach(fn => {
                                    fn.call(element);
                                });
                            });
                        });
                } else {
                    this.on(type[len], handler);
                }
            }
        });
    };

    return DOMNode;
})($);

export default DOMNode;