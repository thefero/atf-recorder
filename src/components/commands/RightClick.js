import Command from '../Command';

const RightClick = (() => {
    const COMMAND_NAME = 'Right clicks'
        , DEFAULT_OPTIONS = {};

    class RightClick extends Command {
        constructor(element) {
            super(element);

            return this;
        }
    }

    return RightClick;
})();

export default RightClick;