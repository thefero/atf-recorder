import Command from '../Command';

const ClickWithLeftButton = (() => {
    const COMMAND_NAME = 'Click with left button'
        , DEFAULT_OPTIONS = {};

    class ClickWithLeftButton extends Command {
        constructor(element) {
            super(element);

            return this;
        }
    }

    return ClickWithLeftButton;
})();

export default ClickWithLeftButton;