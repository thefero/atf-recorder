import Command from '../Command';

const DoubleClick = (() => {
    const COMMAND_NAME = 'Double click'
        , DEFAULT_OPTIONS = {};

    class DoubleClick extends Command {
        constructor(element) {
            super(element);

            return this;
        }
    }

    return DoubleClick;
})();

export default DoubleClick;