import $ from 'jquery';

const Popover = (() => {

    const TEMPLATE_CLASSES = {
        popover: 'recorder-popover'
        , popoverHeader: 'recorder-popover-header'
        , popoverBody: 'recorder-popover-body'
        , popoverFooter: 'recorder-popover-footer'

        , popoverViewport: 'recorder-viewport'
        , popoverViewportFullBanner: 'recorder-viewport-full-banner'
        , popoverViewportPillBanner: 'recorder-viewport-banner-pill'
        , popoverViewportContent: 'recorder-viewport-content'
    }

    const TEMPLATE = `<div class="${TEMPLATE_CLASSES.popover} d-flex items-end flex-column">
        <div class="${TEMPLATE_CLASSES.popoverHeader} d-flex">Atf Recorder</div>
        <div class="${TEMPLATE_CLASSES.popoverBody}">
            <div class="${TEMPLATE_CLASSES.popoverViewport}">
                <div class="${TEMPLATE_CLASSES.popoverViewportFullBanner}"></div>
                <div class="${TEMPLATE_CLASSES.popoverViewportPillBanner}">
                    <h6 class="card-title">NAV <small class="text-muted">body > nav</small></h6>
                </div>
                <div class="${TEMPLATE_CLASSES.popoverViewportContent}"></div>
            </div>
        </div>
        <div class="${TEMPLATE_CLASSES.popoverFooter}"></div>
    </div>`;


    class Popover {
        constructor() {
            this._template = $(TEMPLATE);
            this._commands = [];

            this._popoverViewport = this._template.find(`.${TEMPLATE_CLASSES.popoverViewport}`);

            this._buildLiteners();
        }

        setElement(element) {
            this._element = element;
            this._popoverViewport.find(`.${TEMPLATE_CLASSES.popoverViewportFullBanner}`)
                .append(element);
        }

        addCommand(command) {
            this._commands.push(command);

            this._popoverViewport
                .append(command.pill());

            this._popoverViewport
                .animate({
                    scrollTop: this._popoverViewport[0].scrollHeight
                }, 300);
        }

        get commands() {
            return this._commands;
        }

        get template() {
            return this._template[0];
        }


        // Private
        _buildLiteners() {
            this._popoverViewport.on('scroll.recorder', (ev) => {
                let bannerVisible = $(`.${TEMPLATE_CLASSES.popoverViewportFullBanner}`).outerHeight() - 
                    (
                        this._popoverViewport.offset().top -
                        $(`.${TEMPLATE_CLASSES.popoverViewportFullBanner}`).offset().top
                    ) > 0;

                if (!this._pillBanner) {
                    this._pillBanner = $(`.${TEMPLATE_CLASSES.popoverViewportPillBanner}`);
                }

                if (!bannerVisible && !this._pillBanner.is(":visible")) {
                    this._pillBanner.show(200, () => {
                        this._pillBanner.addClass("recorder-sticky")
                    });
                } else if (bannerVisible && this._pillBanner.is(":visible")) {
                    this._pillBanner.hide(200, function() {
                        this._popoverViewport.animate({
                            scrollTop: 0
                        }, 100, () => {
                            this._popoverViewport.stop();
                        });
                    }.bind(this));
                }
            });
        }
    }

    return Popover;

})();

export default Popover;