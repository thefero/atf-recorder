const path = require('path');

module.exports = {
    assetsPublicPath: '/'
    , assetsSubDirectory: 'static'
    , indexHtml: path.resolve(__dirname, '../dist/index.html')
}