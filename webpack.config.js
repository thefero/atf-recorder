const path = require('path')
    , config = require('./config')
    , utils = require('./build/utils')
    , webpack = require('webpack')
    , ExtractTextPlugin = require('extract-text-webpack-plugin')
    , CopyWebpackPlugin = require('copy-webpack-plugin')
    , HtmlWebpackPlugin = require('html-webpack-plugin')
    , CleanWebpackPlugin = require('clean-webpack-plugin');

const webpackConfig = {
    entry: {
        app: './src/main.js'
    }
    , output: {
        path: path.resolve(__dirname, './dist')
        , filename: utils.assetsPath('js/[name].js')
        , publicPath: config.assetsPublicPath
    }
    , module: {
        rules: [
            {
                test: /\.js$/
                , include: [/src/]
                , loaders: 'babel-loader'
            }
            , {
                test: /\.(css|scss)$/
                , use: ExtractTextPlugin.extract({
                    fallback: 'style-loader'
                    , use: [{
                        loader: 'css-loader'
                        , options: {
                            minimize: process.env.NODE_ENV === 'production'
                        }
                    }, {
                        loader: 'sass-loader'
                        , options: {
                            minimize: process.env.NODE_ENV === 'production'
                        }
                    }]
                })
            }
            , {
                test: /\.(html|ejx)$/
                , loader: 'ejs-loader'
            }, {
                test: /\.(png|jpe?g|gif|svg)(\?.*)?$/
                , loader: 'url-loader'
                , options: {
                    limit: 10000
                    , name: utils.assetsPath('img/[name].[ext]')
                }
            }
            , {
                test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/
                , loader: 'url-loader'
                , options: {
                    limit: 10000
                    , name: utils.assetsPath('media/[name].[ext]')
                }
            }
            , {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/
                , loader: 'url-loader'
                , options: {
                    limit: 10000
                    , name: utils.assetsPath('fonts/[name].[ext]')
                }
            }
        ]
    }
    , plugins: [
        new CleanWebpackPlugin([ 'dist' ])
        , new webpack.ProvidePlugin({
            $: 'jquery'
            , jQuery: 'jquery'
            , 'window.jQuery': 'jquery'
            , Popper: [ 'popper.js', 'default' ]
        })
        , new CopyWebpackPlugin([
            {
                from: path.resolve(__dirname, 'static')
                , to: 'static'
                , ignore: ['.*']
            }
        ])
        , new ExtractTextPlugin("static/css/[name].css")
        , new HtmlWebpackPlugin({
            title: 'My awesome app'
            , appName: require(path.resolve(__dirname, './package.json')).name || 'front-end-starter'
            , filename: config.indexHtml
            , template: './index.html'
            , inject: true
            , useShortDoctype: true
            , minify: {
                removeComments: true,
                collapseWhitespace: false,
                removeAttributeQuotes: true
            }
        })
    ]
};

module.exports = webpackConfig;